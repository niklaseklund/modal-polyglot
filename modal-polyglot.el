;;; modal-polyglot.el --- Modal editing for polyglots -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2022 Niklas Eklund

;; Author: Niklas Eklund <niklas.eklund@posteo.net>
;; URL: https://www.gitlab.com/niklaseklund/modal-polyglot.git
;; Version: 0.1
;; Package-Requires: ((emacs "26.1"))
;; Keywords: convenience

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package allows the user to have a buffer-local insert input layout.
;; The package always switches back to `modal-polyglot-normal-keyboard-layout'
;; in normal mode.

;;; Code:

;;;; Requirements

(require 'subr-x)

;;;; Variables

(defcustom modal-polyglot-enter-hook nil
  "Hook which `modal-polyglot-enter-insert-mode' will be added to."
  :type 'symbol
  :group 'modal-polyglot)

(defcustom modal-polyglot-exit-hook nil
  "Hook which `modal-polyglot-exit-insert-mode' will be added to."
  :type 'symbol
  :group 'modal-polyglot)

(defvar modal-polyglot-insert-keyboard-layout nil
  "Keyboard layout in insert-mode.")
(defvar modal-polyglot-normal-keyboard-layout  "us"
  "Keyboard layout in non-insert-mode.")
(defconst modal-polyglot-program "xkb-switch"
  "Program used for layout switching.")

(make-variable-buffer-local 'modal-polyglot-insert-keyboard-layout)
(make-variable-buffer-local 'modal-polyglot-normal-keyboard-layout)

;;;; Functions

(defun modal-polyglot-enter-insert-mode ()
  "Switch to keyboard layout in `modal-polyglot-insert-keyboard-layout'."
  (when modal-polyglot-insert-keyboard-layout
    (start-process "modal-polyglot-enter" nil modal-polyglot-program
                   "-s" modal-polyglot-insert-keyboard-layout)))

(defun modal-polyglot-exit-insert-mode ()
  "Switch to keyboard layout in `modal-polyglot-normal-keyboard-layout'.
Also store current keyboard layout in `modal-polyglot-insert-keyboard-layout'."
  (let ((buffer-name " *modal-polyglot*")
        (layout))
    (set-process-sentinel
     (start-process "modal-polyglot-exit1" buffer-name
                    modal-polyglot-program "-p")
     (lambda (_process _event)
       (with-current-buffer buffer-name
         (setq layout (string-trim (buffer-string)))
         (erase-buffer))
       (setq modal-polyglot-insert-keyboard-layout layout)
       (start-process "modal-polyglot-exit2" nil modal-polyglot-program
                      "-s" modal-polyglot-normal-keyboard-layout)))))

;;;; Minor mode

(defvar modal-polyglot-mode-map (make-sparse-keymap)
  "Keymap used in `modal-polyglot-mode'.")

;;;###autoload
(define-minor-mode modal-polyglot-mode
  "Toggle Modal Polyglot mode."
  :group 'modal-polyglot
  :keymap modal-polyglot-mode-map
  (if (executable-find modal-polyglot-program)
      (if modal-polyglot-mode
          (progn
            (add-hook modal-polyglot-enter-hook #'modal-polyglot-enter-insert-mode nil t)
            (add-hook modal-polyglot-exit-hook #'modal-polyglot-exit-insert-mode nil t))
        (remove-hook modal-polyglot-enter-hook #'modal-polyglot-enter-insert-mode t)
        (remove-hook modal-polyglot-exit-hook #'modal-polyglot-exit-insert-mode t))
    (warn (format "Program %s not installed" modal-polyglot-program))))

(provide 'modal-polyglot)

;;; modal-polyglot.el ends here
