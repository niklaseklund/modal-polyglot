;;; guix.scm -- Guix package definition

(use-modules
 (guix packages)
 (guix git-download)
 (guix gexp)
 (guix build-system gnu)
 ((guix licenses) #:prefix license:)
 (guix build-system emacs)
 (gnu packages emacs-xyz)
 (ice-9 popen)
 (ice-9 rdelim))

(define %source-dir (dirname (current-filename)))

(define %git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f2" OPEN_READ)))

(define-public emacs-modal-polyglot
  (let ((branch "master")
        (commit "47961d5a914e34196040e758405f2fdf76acbf94")
        (revision "0"))
    (package
      (name "emacs-modal-polyglot")
      (version (git-version "0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/niklaseklund/modal-polyglot")
               (commit commit)))
         (sha256
          (base32
           "1sy355sj6a0s7xj29nkb6a6w7zx5wxa5gxnhxmc5ijlwi59dzgf8"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://gitlab.com/niklaseklund/modal-polyglot")
      (synopsis "Modal editing for polyglots")
      (description "A convenient package for polyglots who use modal editing.")
      (license license:gpl3+))))

(package
  (inherit emacs-modal-polyglot)
  (name "emacs-modal-polyglot-git")
  (version (git-version (package-version emacs-modal-polyglot) "HEAD" %git-commit))
  (source (local-file %source-dir
                      #:recursive? #t)))
